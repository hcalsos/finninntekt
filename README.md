Finn Inntekt 
============

Prerequisites :

Du trenger en keystore som inneholder de nødvendige sertifikatene for tjenesten.

- eget virksomhetssertifkat (her definert som 'certalias')
- tjenestetilbyders sertifikater

Eksempelet her er satt til bruke /usr/local/pki/finnInntektKeystore.jks
Dette kan endres i keystore.properties filene.

finnInntektSTS modulen lager en war fil som deployes til en container.

finnInntektAPI modulen har en test som kan kjøre for å hente ut stub fra tjenesten hos SKE (InntektDataResourceIT)

finnInntektWSDL inneholder en modifisert wsdl fra SKE som definerer SAML2 bearer transport policy