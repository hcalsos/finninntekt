package org.example.finnInntekt;


import org.apache.wss4j.common.ext.WSPasswordCallback;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;
import java.io.IOException;

public class PasswordCallbackHandler implements CallbackHandler {
    @Override
    public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
        for (Callback callback : callbacks) {
            if (callback instanceof WSPasswordCallback) {
                WSPasswordCallback pc = (WSPasswordCallback) callback;
                if (pc.getUsage() == WSPasswordCallback.DECRYPT ||
                        pc.getUsage() == WSPasswordCallback.SIGNATURE) {
                    if ("certalias".equals(pc.getIdentifier())) {
                        pc.setPassword("changeit");
                    }
                }
            }
        }
    }
}