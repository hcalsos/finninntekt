package org.example.finnInntekt;

import org.apache.cxf.rt.security.claims.Claim;
import org.apache.cxf.rt.security.claims.ClaimCollection;
import org.apache.cxf.sts.claims.ClaimsHandler;
import org.apache.cxf.sts.claims.ClaimsParameters;
import org.apache.cxf.sts.claims.ProcessedClaim;
import org.apache.cxf.sts.claims.ProcessedClaimCollection;

import java.net.URI;
import java.util.Arrays;
import java.util.List;

public class CustomClaimHandler implements ClaimsHandler {

    private static final URI SLUTTBRUKER = URI.create("sluttbruker");
    private static final URI SYSTEMBRUKER = URI.create("systembruker");

    @Override
    public List<URI> getSupportedClaimTypes() {
        return Arrays.asList(SLUTTBRUKER, SYSTEMBRUKER);
    }

    @Override
    public ProcessedClaimCollection retrieveClaimValues(ClaimCollection claims, ClaimsParameters parameters) {
        ProcessedClaimCollection processedClaims = new ProcessedClaimCollection();
        for (Claim claim : claims) {
            ProcessedClaim processedClaim = new ProcessedClaim();
            processedClaim.setClaimType(claim.getClaimType());
            processedClaim.setOptional(claim.isOptional());
            processedClaim.setValues(claim.getValues());
            processedClaim.setPrincipal(parameters.getPrincipal());
            processedClaims.add(processedClaim);
        }
        return processedClaims;
    }
}