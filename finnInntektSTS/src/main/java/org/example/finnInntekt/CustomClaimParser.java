package org.example.finnInntekt;

import lombok.extern.slf4j.Slf4j;
import org.apache.cxf.rt.security.claims.Claim;
import org.apache.cxf.sts.claims.ClaimsParser;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.net.URI;
import java.net.URISyntaxException;

@Slf4j
public class CustomClaimParser implements ClaimsParser {

    public static final String CLAIMS_DIALECT = "";

    @Override
    public Claim parse(Element claim) {
        String localName = claim.getLocalName();
        String claimNS = claim.getNamespaceURI();
        if ("ClaimType".equals(localName) || "ClaimValue".equals(localName)) {
            String uri = claim.getAttributeNS(null, "Uri");
            String optional = claim.getAttributeNS(null, "Optional");

            Claim requestClaim = new Claim();
            try {
                requestClaim.setClaimType(new URI(uri));
            } catch (URISyntaxException e) {
                log.warn("Kan ikke lage URI av ClaimType attributtverdi " + uri);
            }
            requestClaim.setOptional(Boolean.parseBoolean(optional));
            if ("ClaimValue".equals(localName)) {
                Node value = claim.getFirstChild();
                if (value != null) {
                    if ("Value".equals(value.getLocalName())) {
                        requestClaim.addValue(value.getTextContent().trim());
                    } else {
                        log.warn("Usupportert subelement i ClaimValue : " + value.getLocalName());
                    }
                } else {
                    log.warn("Fant ikke subelement i ClaimValue");
                    return null;
                }
            }
            return requestClaim;
        }
        log.trace("Fant ukjent element : {} {}", localName, claimNS);
        return null;
    }

    @Override
    public String getSupportedDialect() {
        return CLAIMS_DIALECT;
    }
}
