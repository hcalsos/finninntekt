package org.example.finnInntekt.callbackhandler;

import org.apache.cxf.helpers.DOMUtils;
import org.apache.cxf.ws.security.trust.claims.ClaimsCallback;
import org.apache.wss4j.dom.WSConstants;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;
import java.io.IOException;

public class ClaimsCallbackHandler implements CallbackHandler {

    private final String identity = "";
    private final String systembruker;
    private final String sluttbruker;

    public ClaimsCallbackHandler(String sluttbruker, String systembruker) {
        this.sluttbruker = sluttbruker;
        this.systembruker = systembruker;
    }


    @Override
    public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
        for (Callback callback : callbacks) {
            if (callback instanceof ClaimsCallback) {
                ClaimsCallback claim = (ClaimsCallback) callback;
                claim.setClaims(createClaims());
            }
        }
    }

    private Element createClaims() {
        Document doc = DOMUtils.createDocument();

        Element claimsElement = doc.createElementNS("http://docs.oasis-open.org/ws-sx/ws-trust/200512", "Claims");
        claimsElement.setAttributeNS(null, "Dialect", identity);

        Element sluttbrukerValue = createClaimsValue(doc, "sluttbruker", sluttbruker);
        claimsElement.appendChild(sluttbrukerValue);

        Element systembrukerValue = createClaimsValue(doc, "systembruker", systembruker);
        claimsElement.appendChild(systembrukerValue);

        return claimsElement;
    }

    private Element createClaimsValue(Document doc, String uri, String value) {
        Element claimValue = doc.createElementNS(identity, "ClaimValue");
        claimValue.setAttributeNS(null, "Uri", identity + uri);
        claimValue.setAttributeNS(WSConstants.XMLNS_NS, "xmlns", identity);
        Element valueNode = doc.createElementNS(identity, "Value");
        valueNode.setTextContent(value);
        claimValue.appendChild(valueNode);
        return claimValue;
    }
}
