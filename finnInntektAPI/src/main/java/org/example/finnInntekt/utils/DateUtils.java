package org.example.finnInntekt.utils;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.GregorianCalendar;

public class DateUtils {

    public static XMLGregorianCalendar toXMLGregorianCalendar(LocalDate dato) throws DatatypeConfigurationException {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(dato.toDate());
        return DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
    }

    public static XMLGregorianCalendar toXMLGregorianCalendar(LocalDateTime dateTime) throws DatatypeConfigurationException {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(dateTime.toDateTime().toDate());
        return DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
    }
}
