package org.example.finnInntekt.resources;

import lombok.extern.slf4j.Slf4j;
import no.skatteetaten.fastsetting.inntektdata.v1.FinnInntektForespoersel;
import no.skatteetaten.fastsetting.inntektdata.v1.FinnInntektRespons;
import no.skatteetaten.fastsetting.inntektdata.v1.IPlattformFeil;
import no.skatteetaten.fastsetting.inntektdata.v1.InntektData;
import no.skatteetaten.fastsetting.inntektdata.v1.InntektData_Service;
import org.apache.cxf.binding.soap.saaj.SAAJInInterceptor;
import org.apache.cxf.binding.soap.saaj.SAAJOutInterceptor;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.ws.security.SecurityConstants;
import org.apache.cxf.ws.security.trust.STSClient;
import org.example.finnInntekt.callbackhandler.ClaimsCallbackHandler;
import org.example.finnInntekt.callbackhandler.ClientCallbackHandler;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import skatteetaten.felles.brukskontekst.v1.BruksKontekstType;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.ws.BindingProvider;
import java.util.HashMap;
import java.util.Map;

import static org.example.finnInntekt.utils.DateUtils.toXMLGregorianCalendar;

@Slf4j
public class InntektDataResource {

    private final String endpoint = "https://int-at.sits.no:22002/fastsetting/inntekt/v1";
    private final String stsWsdlLocation = "http://localhost:8080/finnInntektSTS/STS?wsdl";

    private InntektData ws;

    public InntektDataResource() {
        ws = new InntektData_Service().getInntektDataPort();
        ((BindingProvider) ws).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpoint);
    }

    public FinnInntektRespons finnInntekt(String fnr, LocalDate fradato, LocalDate tildato, String sluttbruker, String systembruker) {
        try {
            Client client = ClientProxy.getClient(ws);
            addInterceptors(client);
            createSTSClient(sluttbruker, systembruker, client);

            FinnInntektForespoersel forespoersel = createRequest(fnr, fradato, tildato);
            BruksKontekstType brukskontekst = createBruksKontekst();

            return ws.finnInntekt(forespoersel, brukskontekst);
        } catch (DatatypeConfigurationException | IPlattformFeil e) {
            log.error("Error creating/sending request to " + endpoint, e);
        }
        return null;
    }

    private FinnInntektForespoersel createRequest(String fnr, LocalDate fradato, LocalDate tildato) throws DatatypeConfigurationException {
        FinnInntektForespoersel request = new FinnInntektForespoersel();
        request.setNorskIdentifikator(fnr);
        request.setFraOgMed(toXMLGregorianCalendar(fradato));
        request.setTilOgMed(toXMLGregorianCalendar(tildato));
        return request;
    }

    private BruksKontekstType createBruksKontekst() throws DatatypeConfigurationException {
        BruksKontekstType brukskontekst = new BruksKontekstType();
        brukskontekst.setMeldingID(String.valueOf(System.currentTimeMillis())); // unique messageId
        brukskontekst.setTidspunkt(toXMLGregorianCalendar(new LocalDateTime()));
        brukskontekst.setMiljo("TEST");
        brukskontekst.setTestData(false);
        brukskontekst.setKundeID("kundeID");
        brukskontekst.setBrukerID("brukerID");
        return brukskontekst;
    }

    private void createSTSClient(String sluttbruker, String systembruker, Client client) {
        STSClient stsClient = new STSClient(client.getBus());
        stsClient.setWsdlLocation(stsWsdlLocation);
        stsClient.setServiceName("{http://docs.oasis-open.org/ws-sx/ws-trust/200512/}SecurityTokenService");
        stsClient.setEndpointName("{http://docs.oasis-open.org/ws-sx/ws-trust/200512/}STS_Port");
        stsClient.setClaimsCallbackHandler(new ClaimsCallbackHandler(sluttbruker, systembruker));
        stsClient.setProperties(createSTSProperties());

        ((BindingProvider) ws).getRequestContext().put("ws-security.sts.client", stsClient);
    }

    private Map<String, Object> createSTSProperties() {
        Map<String, Object> stsProperties = new HashMap<>();
        stsProperties.put(SecurityConstants.SIGNATURE_USERNAME, "certalias");
        stsProperties.put(SecurityConstants.SIGNATURE_PROPERTIES, "keystore.properties");
        stsProperties.put(SecurityConstants.CALLBACK_HANDLER, ClientCallbackHandler.class.getName());
        stsProperties.put(SecurityConstants.ENCRYPT_USERNAME, "certalias");
        stsProperties.put(SecurityConstants.ENCRYPT_PROPERTIES, "keystore.properties");
        stsProperties.put(SecurityConstants.STS_TOKEN_USERNAME, "certalias");
        stsProperties.put(SecurityConstants.STS_TOKEN_PROPERTIES, "keystore.properties");
        return stsProperties;
    }

    private void addInterceptors(Client client) {
        // In interceptors
        LoggingInInterceptor inInterceptor = new LoggingInInterceptor();
        inInterceptor.setPrettyLogging(true);
        client.getInInterceptors().add(inInterceptor);
        client.getEndpoint().getInInterceptors().add(new SAAJInInterceptor());
        // Out interceptors
        LoggingOutInterceptor outInterceptor = new LoggingOutInterceptor();
        outInterceptor.setPrettyLogging(true);
        client.getOutInterceptors().add(outInterceptor);
        client.getEndpoint().getOutInterceptors().add(new SAAJOutInterceptor());
    }
}
