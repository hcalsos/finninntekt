import no.skatteetaten.fastsetting.inntektdata.v1.FinnInntektRespons;
import org.example.finnInntekt.resources.InntektDataResource;
import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class InntektDataResourceIT {

    private InntektDataResource inntektDataResource;

    @Before
    public void setUp() throws Exception {
        inntektDataResource = new InntektDataResource();
    }

    @Test
    public void verify_lookup() throws Exception {
        String fnr = "1235678910";
        LocalDate fradato = new LocalDate(2015, 10, 1);
        LocalDate tildato = fradato.plusMonths(1);
        String testbruker = "testbruker";
        String testsystem = "testsystem";
        FinnInntektRespons inntekt = inntektDataResource.finnInntekt(fnr, fradato, tildato, testbruker, testsystem);
        assertNotNull(inntekt);
        assertThat(inntekt.getInntekt().size(), is(59));
    }

}
